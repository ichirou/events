Django==1.9.8
djangorestframework==3.4.1
drfdocs==0.0.11
pytz==2016.6.1
