from django.db import models
from django.utils import timezone

class Event(models.Model):
    key = models.CharField(max_length=128)
    #when the event will take place?
    timestamp = models.DateTimeField(default=timezone.now)
    metadata = models.CharField(max_length=128, blank=True)
    owner = models.ForeignKey('auth.User', related_name='events')
    #when was the event itself created by the user?
    created = models.DateTimeField(default=timezone.now)

    def __str__(self):  # __unicode__ on Python 2
        return self.key