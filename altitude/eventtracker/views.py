from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework import generics, permissions, status
from .models import Event
from .serializers import EventSerializer, UserSerializer
from .permissions import IsOwnerOrReadOnly
from django.utils import timezone
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from .forms import UserForm
from .models import Event

import datetime, random

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'events': reverse('event-list', request=request, format=format),
    })

def dashboard(request):
    context = RequestContext(request)
    events = Event.objects.all().order_by('created')

    return render_to_response("eventtracker/dashboard.html", {'events': events}, context)

def register(request):
    # Like before, get the request's context.
    context = RequestContext(request)

    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)

        # If the two forms are valid...
        if user_form.is_valid():
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Update our variable to tell the template registration was successful.
            registered = True

        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print (user_form.errors)
    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()

    # Render the template depending on the context.
    return render_to_response(
            'eventtracker/register.html',
            {'user_form': user_form, 'registered': registered},
            context)

def user_login(request):
    # Like before, obtain the context for the user's request.
    context = RequestContext(request)

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            print
            "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('eventtracker/login.html', {}, context)

# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/')

class UserList(generics.ListAPIView):
    """
    List all users.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    """
    Shows detail of a user.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

class EventList(APIView):
    """
    Lists all events.\n
    Can create one or more events.\n
    Can delete events.\n
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = EventSerializer

    def get(self, request, format=None):
        events = Event.objects.all()
        key = self.request.query_params.get('key', None)
        start_timestamp = self.request.query_params.get('start_timestamp', None)
        end_timestamp = self.request.query_params.get('end_timestamp', None)

        if key and start_timestamp and end_timestamp is not None:
            events = events.filter(key=key, timestamp__gte=start_timestamp, timestamp__lte=end_timestamp)
        elif start_timestamp and end_timestamp is not None:
            events = events.filter(timestamp__gte=start_timestamp, timestamp__lte=end_timestamp)
        elif key is not None:
            events = events.filter(key=key)

        events = events.order_by('timestamp')
        serializer = EventSerializer(events, many=True, context={'request': request})

        return Response(serializer.data)

    def put(self, request, format=None):
        # sample put: http -a admin:pass1234 put http://127.0.0.1:8000/events/ owner=1 key="something" timestamp="2016-08-01T05:58:26"
        serializer = EventSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(owner=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        # #sample post: [{"key": "EVENT3","metadata": "NUMBER 3 EVENT"},{"key": "EVENT4","metadata": "NUMBER 4 EVENT"}]
        #support management command for creating count number of events with key specified and random sequential time stamp
        #http -a admin:pass1234 post "http://127.0.0.1:8000/events/?key=MNYEVT&count=3"
        key = self.request.query_params.get('key', None)
        count = self.request.query_params.get('count', None)

        if key and count is not None:
            count = int(count)
            final_timestamp = timezone.now()
            current_datetime = timezone.now().strftime("%Y-%m-%d %H:%M:%S")

            while count > 0:
                final_timestamp = final_timestamp + datetime.timedelta(minutes=random.randint(1,360))
                event = Event(key=key, timestamp=final_timestamp, owner=request.user, created=current_datetime, metadata=key)
                event.save()
                count -= 1

            print (current_datetime)
            events = Event.objects.all().filter(key=key, created=current_datetime)
            serializer = EventSerializer(events, many=True, context={'request': request})
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        #support for creating multiple events with json input
        else:
            serializer = EventSerializer(data=request.data, context={'request': request}, many=True)
            if serializer.is_valid():
                serializer.save(owner=request.user)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        events = Event.objects.all()
        key = self.request.query_params.get('key', None)
        start_timestamp = self.request.query_params.get('start_timestamp', None)
        end_timestamp = self.request.query_params.get('end_timestamp', None)

        if key and start_timestamp and end_timestamp is not None:
            events = events.filter(key=key, timestamp__gte=start_timestamp, timestamp__lte=end_timestamp)
        elif start_timestamp and end_timestamp is not None:
            events = events.filter(timestamp__gte=start_timestamp, timestamp__lte=end_timestamp)
        elif key is not None:
            events = events.filter(key=key)

        events.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

class EventDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Shows detail of an event.
    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)