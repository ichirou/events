from .models import Event
from django.contrib.auth.models import User
from django import forms
from django.utils import timezone

class UserForm(forms.ModelForm):
    username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Username', 'autofocus' : ''}))
    email = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Email'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class' : 'form-control', 'placeholder' : 'Password'}))

    class Meta:
        model = User
        fields = ['username', 'email', 'password']