from django.test import TestCase
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Event
from django.contrib.auth.models import User

class EventTests(APITestCase):
    def setUp(self):
        user = User.objects.create_superuser(username='admin', email='admin@example.com', password='pass1234')
        Event.objects.create(key="TESTEVENT1", metadata="test event 1", owner=user)
        #Event.objects.create(key="TESTEVENT2", metadata="test event 2", timestamp="2016-08-04T15:59:32")
        #Event.objects.create(key="TESTEVENT2", timestamp="2016-08-04T15:59:32")

    def test_get_events(self):
        url = reverse('event-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_event_1(self):
        response = self.client.get('/events/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_event(self):
        url = reverse('event-list')
        user = User.objects.get(username='admin')
        data = {
            'key': 'ONEVENT',
            'metadata': 'one event',
            'owner': user,
        }
        self.client.force_authenticate(user=user)
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        #self.assertEqual(Event.objects.get(key='ONEVENT'))