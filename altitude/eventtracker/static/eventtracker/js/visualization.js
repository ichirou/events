function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function hideTimelineInputErrors() {
    $("#keyform").attr("class", "form-group");
    $("#keyhelp").attr("style", "display:none;");
    $("#start_date_form").attr("class", "form-group");
    $("#startdatehelp").attr("style", "display:none;");
    $("#end_date_form").attr("class", "form-group");
    $("#enddatehelp").attr("style", "display:none;");
}

$(document).ready(function () {
    var timeline;
    var url = window.location.href;
    url = url.replace('dashboard/','');

    $.getJSON(url + "events", function(data) {
        var parsedEvents = {
        events : []
        };
        var JSONdata = {};

        for (i = 0; i < data.length; i++) {
            JSONdata["id"] = i;
            JSONdata["content"] = data[i].key;
            JSONdata["start"] = data[i].timestamp;
            JSONdata["title"] = data[i].metadata;
            parsedEvents.events.push(JSONdata)
            JSONdata = {};
        }

        //create the timeline graph
        var container = document.getElementById('visualization');
        var items = new vis.DataSet(parsedEvents.events);
        var options = {};
        timeline = new vis.Timeline(container, items, options);
    });

    $.getJSON(url + "users", function(data) {
        var labelsArray = [];
        var eventsArray = [];
        var colorsArray = [];

        for (i = 0; i < data.results.length; i++) {
            labelsArray[i] = data.results[i].username;
            eventsArray[i] = data.results[i].events.length
            colorsArray[i] = getRandomColor();
        }
        var barChartElement = document.getElementById("barChart");
        var pieChartElement = document.getElementById("pieChart");

        var myChart = new Chart(barChartElement, {
            type: 'bar',
            data: {
                labels: labelsArray,
                datasets: [{
                    label: "events",
                    data: eventsArray,
                    backgroundColor: colorsArray,
                    borderColor: colorsArray,
                    borderWidth: 1,
                }]
            },
        });

        var pieChart = new Chart(pieChartElement, {
            type: 'pie',
            data: {
                labels: labelsArray,
                datasets: [
                    {
                        data: eventsArray,
                        backgroundColor: colorsArray,
                    }]
            }
        });
    });

    $("#update_graph").click(function(e){
        e.preventDefault();
        var key = $("#key").val();
        var start_timestamp = $("#start_timestamp").val();
        var end_timestamp = $("#end_timestamp").val();
        var parsedEvents = {
            events : []
        };
        var JSONdata = {};
        var container = document.getElementById('visualization');
        var options = {};

        if (!$("#start_timestamp").val() && !$("#end_timestamp").val()){
            if(!$("#key").val()) {
                //alert("key is missing, at least put in key");
                $("#keyform").attr("class", "form-group has-error");
                $("#keyhelp").attr("style", "display:true;");
                $("#keyhelp").text("At least input an event key if no timestamp specified!");
            }
            else {
                hideTimelineInputErrors();
                $.getJSON(url + "events/" + "?key=" + key, function(data) {
                    for (i = 0; i < data.length; i++) {
                        JSONdata["id"] = i;
                        JSONdata["content"] = data[i].key;
                        JSONdata["start"] = data[i].timestamp;
                        parsedEvents.events.push(JSONdata)
                        JSONdata = {};
                    }
                    var items = new vis.DataSet(parsedEvents.events);
                    timeline.destroy();
                    timeline = new vis.Timeline(container, items, options);
                });
            }
        }
        else if(!$("#start_timestamp").val() || !$("#end_timestamp").val()) {
            if(!$("#start_timestamp").val()) {
                $("#start_date_form").attr("class", "form-group has-error");
                $("#startdatehelp").attr("style", "display:true;");
                $("#startdatehelp").text("Both timestamps are needed!");
            }
            else {
                $("#end_date_form").attr("class", "form-group has-error");
                $("#enddatehelp").attr("style", "display:true;");
                $("#enddatehelp").text("Both timestamps are needed!");
            }
        }
        else {
            if(!$("#key").val()) {
                hideTimelineInputErrors();
                $.getJSON(url + "events/" + "?start_timestamp=" + start_timestamp
                    + "&end_timestamp=" + end_timestamp, function(data) {
                    for (i = 0; i < data.length; i++) {
                        JSONdata["id"] = i;
                        JSONdata["content"] = data[i].key;
                        JSONdata["start"] = data[i].timestamp;
                        parsedEvents.events.push(JSONdata)
                        JSONdata = {};
                    }
                    var items = new vis.DataSet(parsedEvents.events);
                    timeline.destroy();
                    timeline = new vis.Timeline(container, items, options);
                });
            }
            else {
                hideTimelineInputErrors();
                $.getJSON(url + "events/" + "?key=" + key + "&start_timestamp=" + start_timestamp
                    + "&end_timestamp=" + end_timestamp, function(data) {
                    for (i = 0; i < data.length; i++) {
                        JSONdata["id"] = i;
                        JSONdata["content"] = data[i].key;
                        JSONdata["start"] = data[i].timestamp;
                        parsedEvents.events.push(JSONdata)
                        JSONdata = {};
                    }
                    var items = new vis.DataSet(parsedEvents.events);
                    timeline.destroy();
                    timeline = new vis.Timeline(container, items, options);
                });
            }
        }
    });
});