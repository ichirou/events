from django.apps import AppConfig

class EventTrackerConfig(AppConfig):
    name = 'eventtracker'
