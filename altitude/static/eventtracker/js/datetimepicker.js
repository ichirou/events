$.datetimepicker.setDateFormatter({
    parseDate: function (date, format) {
        var d = moment(date, format);
        return d.isValid() ? d.toDate() : false;
    },

    formatDate: function (date, format) {
        return moment(date).format(format);
    }
});

jQuery('#start_timestamp').datetimepicker({
    format:'YYYY-MM-DD' + 'T' + 'hh:mm',
});

jQuery('#end_timestamp').datetimepicker({
    format:'YYYY-MM-DD' + 'T' + 'hh:mm',
});