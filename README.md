# README #

### Event Tracker ###

AWS deployed application: [http://eventtracker-env.dzvvf3pgxh.us-west-2.elasticbeanstalk.com/](http://eventtracker-env.dzvvf3pgxh.us-west-2.elasticbeanstalk.com/)

* Username: admin
* Password: pass1234

Bitbucket source code:

* [https://bitbucket.org/ichirou/events](https://bitbucket.org/ichirou/events)
* [https://ichirou@bitbucket.org/ichirou/events.git](https://ichirou@bitbucket.org/ichirou/events.git)


### Local Install Instructions ###

Summary of local set up (Windows)

1. Install Git:

    * [https://git-scm.com/downloads](https://git-scm.com/downloads)
    * Follow on-screen instructions for installation

1. Clone the repository:

    * Open a command-line window and create the project directory

        `mkdir events`

        `cd events`

    * Clone the repository using the command below

        `git clone https://bitbucket.org/ichirou/events.git`

1. Install Python:

    * [https://www.python.org/downloads/](https://www.python.org/downloads/) (Python 3.5 recommended)
    * Follow on-screen instructions
    * Make sure Python scripts and directory is set in your PATH environment variable (directory name will be different for different version of python)

        `...\Python\Python35-32\Scripts\`

        `...\Python\Python35-32\`

    * Enter `python --version` in command line to check if python is installed correctly.

1. Install virtualenv package and virtualenv wrapper

    * `pip install virtualenv`
    * `pip install virtualenvwrapper-win`

1. Setup and activate virtual environment

    * `mkvirtualenv events`
    * Command-line will have a name (events) to indicate successful virtual environment activation
    * To deactivate virtual environment: `deactivate`
    * To work on existing environment: `workon events`

1. Install dependencies:

    * Just use `pip install -r requirements.txt`

1. Update migration of database

    * `cd altitude`

    * `python manage.py makemigrations`

    * `python manage.py migrate`

1. You can create your own admin account for administration login and API access

    * `python manage.py createsuperuser`

1. Run the server

    * `python manage.py runserver`

    * Open [http://127.0.0.1:8000/](http://127.0.0.1:8000/) to access the web application

###API###

1. Django REST framework: [http://www.django-rest-framework.org/](http://www.django-rest-framework.org/)
1. We can use httpie to send http methods and data to the application: `pip install httpie`
1. API endpoints:
    * Root API (browsable): [http://127.0.0.1:8000/api/](http://127.0.0.1:8000/api/)
    * Events API: [http://127.0.0.1:8000/events/](http://127.0.0.1:8000/events/)
        * To get list of events: `http get http://127.0.0.1:8000/events/`
        * To get detail of event with pk = 1: `http get http://127.0.0.1:8000/events/1/`
        * To create an event: `http -a admin:pass1234 put http://127.0.0.1:8000/events/ key="TESTKEY" metadata="test event" timestamp="2016-08-03T15:59:32"`
        * To query events according to key and specified start and end timestamps using query parameters on the URL: `http get http://127.0.0.1:8000/events/?key=EXSTNGKEY&start_timestamp=2016-08-03T15:59:32&end_timestamp=2016-08-03T15:59:32`
        * To create multiple events, POST a list of JSON objects, save the JSON list first in a file data.json for example: `data.json: [{"key":"EVENT3","metadata":"NUMBER 3 EVENT"},{"key":"EVENT4","metadata":"NUMBER 4 EVENT"}]`
`http -a admin:pass1234 post http://127.0.0.1:8000/events/ < data.json`
       * To create 'count' number of events with a specified key that will generate events with random sequential timestamps of 1-360 minutes: `http -a admin:pass1234 post http://127.0.0.1:8000/events/?key=MULTIEVENT&count=10`
       * To delete all events: `http -a admin:pass1234 delete http://127.0.0.1:8000/events/`
       * To delete all events with a specified key: `http -a admin:pass1234 delete http://127.0.0.1:8000/events/?key=EVENTSTODELETE`


1. Basic API documentation using DRF docs(under experimentation): [http://127.0.0.1:8000/docs/](http://127.0.0.1:8000/docs/)

1. API Notes:
    * Users need to be authenticated (logged in) to use PUT, POST and DELETE http methods to the events API.
    * In creating an event, when timestamp is not specified, event will be created with timestamp = current date and time. Metadata is optional.
    * Users can only delete and edit their own individual events, but then can delete multiple events even if the event is not theirs.
    * API endpoint support JSON data format.
    * User cannot delete other user's events individually, though a user can delete multiple events even if the event does not belong to them.

###Dashboard###

1. URL: [http://127.0.0.1:8000/dashboard/](http://127.0.0.1:8000/dashboard/)
1. Frameworks used:
    * Boostrap: [http://getbootstrap.com/](http://getbootstrap.com/)
    * vis.js: [http://visjs.org/](http://visjs.org/)
    * Charts.js: [http://www.chartjs.org/](http://www.chartjs.org/)
1. User can specify Event key, start and end timestamps to filter events displayed on the time line graph.
1. User can zoom in and out of the time line for better view.

###Deployment Mechanism###
  * I am using Codeship for automatic deployment to AWS Elastic Beanstalk: [https://codeship.com/](https://codeship.com/)
  * Everytime I push my code to bitbucket, codeship will automatically pull the latest code, run tests, then deploy to AWS EB.
  * Make sure to push deployment settings first. (copy contents of settings_production.py and push to repo.
  * [https://codeship.com/projects/166870](https://codeship.com/projects/166870): I can add you to the codeship project, just contact me and inform me of your email so I can add you.
  * How to setup?
       * Create AWS EB environment first
           1. Instructions [here](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-django.html)
           1. Make sure requirements.txt is available.
           1. Make sure to run `python manage.py collectstatic`, commit and push the static files.
       * Create a codeship account
       * Create new project and set repository to https://ichirou@bitbucket.org/ichirou/events.git
       * Setup commands: `cd altitude`
`pip install -r requirements.txt`
`python manage.py makemigrations`
`python manage.py migrate`
       * Test commands: `python manage.py test`
       * Deployment settings, fill-in the following settings(made during the creation of AWS EB environment:
         * AWS Access Key ID: xxx
         * AWS Secret Access Key: xxx
         * Region: us-west-2
         * Application: altitude
         * Environment: eventtracker-env
         * S3 Bucket: elasticbeanstalk-us-west-2-562267443237/altitude

### Who do I talk to? ###
* Richmond Ko (richmond.ko@outlook.ph)